/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <functional>
#include <QUrl>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QQmlContext>
#include <QtQml/QQmlApplicationEngine>
#include <cstring>
#include <QFileInfo>

#include <json-c/json.h>

#include "eventhandler.h"

const char _myrole[] = "on_screen";
const char _parameter[] = "parameter";
const char _replyto[] = "replyto";
const char _onscreen_title[] = "onscreenTitle";
const char _button_name[] = "buttonName";
const char _drawing_name[] = "drawing_name";
const char _application_id[] = "application_id";


void *EventHandler::myThis = 0;

EventHandler::EventHandler(QObject *parent) :
	QObject(parent),
	m_dsp_sts(false)
{
	fprintf(stdout, "EventHandler::EventHandler()\n");
}

EventHandler::~EventHandler()
{
	fprintf(stdout, "EventHandler::~EventHandler()\n");
}

void EventHandler::init(int port, const char *token)
{
	myThis = this;

	fprintf(stdout, "EventHandler::init(port=%d, token=%s)\n", port, token);
}

void EventHandler::onRep_static(struct json_object* reply_contents)
{
	(void) reply_contents;
}

void EventHandler::onRep(struct json_object* reply_contents)
{
	(void) reply_contents;
}

void EventHandler::activateWindow(const char *role, const char *area)
{
	fprintf(stdout, "EventHandler::activateWindow(role=%s, area=%s)\n", role, area);
}

void EventHandler::deactivateWindow()
{
	fprintf(stdout, "EventHandler::deactivateWindow()\n");

	if (getDisplayStatus() == SWAPPING) {
		setDisplayStatus(SHOWING);
		m_dsp = m_req;
		updateModel(QVariant(this->m_dsp.second));
		emit showOnScreen();
	} else {
		this->setDisplayStatus(HIDING);
		//mp_wm->deactivateWindow(_myrole);
	}
}

void EventHandler::onScreenReply(const QString &ons_title, const QString &btn_name)
{
	fprintf(stdout, "EventHandler::onScreenReply(), ons_title=%s btn_name=%s\n",
			ons_title.toStdString().c_str(),
			btn_name.toStdString().c_str());

	emit this->hideOnScreen();

	struct json_object* j_param = json_object_new_object();
	json_object_object_add(j_param, _onscreen_title,
			json_object_new_string(ons_title.toStdString().c_str()));

	json_object_object_add(j_param, _button_name,
			json_object_new_string(btn_name.toStdString().c_str()));

	//mp_hs->replyShowWindow(m_dsp.first.toStdString().c_str(), j_param);
}
