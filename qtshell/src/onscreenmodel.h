#ifndef ONSCREENMODEL_H
#define ONSCREENMODEL_H

#include <QObject>
#include <QVariant>
#include <QStringList>


class OnScreenModel : public QObject
{
Q_OBJECT

public:
	explicit OnScreenModel(QObject *parent = nullptr){}
	~OnScreenModel() = default;

public slots:
	QString getTitle(void) const { return m_title; }
	QString getType(void) const { return m_type; }
	QString getContents(void) const { return m_contents; }
	int buttonNum(void) const {return m_buttons.size(); }

	QString buttonName(int index) const;
	void setModel(QVariant data);
	void clearOnScreenModel(void);

private:
	void clearModel(void);

	QString m_title;
	QString m_type;
	QString m_contents;

	QStringList m_buttons;
};

#endif // ONSCREENMODEL_H
