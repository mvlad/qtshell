#include "onscreenmodel.h"

#include <json-c/json.h>

const char _modelName[] = "OnScreenModel";
const char _title[] = "title";
const char _type[] = "type";
const char _contents[] = "contents";
const char _buttons[] = "buttons";


void OnScreenModel::setModel(QVariant data)
{
    clearModel();

    struct json_object *j_title = nullptr,
		       *j_type = nullptr,
		       *j_contents = nullptr,
		       *j_buttons = nullptr;
    struct json_object *j_param =
	    json_tokener_parse(data.toString().toStdString().c_str());

    if (json_object_object_get_ex(j_param, _title, &j_title)) {
	    m_title = json_object_get_string(j_title);
    }

    if (json_object_object_get_ex(j_param, _type, &j_type)) {
	    m_type = json_object_get_string(j_type);
    }

    if (json_object_object_get_ex(j_param, _contents, &j_contents)) {
	    m_contents = json_object_get_string(j_contents);
    }

    if (json_object_object_get_ex(j_param, _buttons, &j_buttons)) {
	    if (json_object_get_type(j_buttons) == json_type_array) {
		    m_buttons.clear();

		    int len = json_object_array_length(j_buttons);
		    struct json_object *json_tmp = nullptr;

		    for (int i = 0; i < len; i++) {
			    json_tmp = json_object_array_get_idx(j_buttons, i);
			    m_buttons << QString(json_object_get_string(json_tmp));
		    }
	    }
    }

    fprintf(stdout, "setModel end!titile=%s,type=%s,contents=%s,btnNum=%d",
	      m_title.toStdString().c_str(), m_type.toStdString().c_str(),
	      m_contents.toStdString().c_str(), m_buttons.size());
}

QString OnScreenModel::buttonName(int index) const
{
	if (index >= m_buttons.size())
		return QString("");
	else
		return m_buttons[index];
}

void OnScreenModel::clearOnScreenModel(void)
{
	fprintf(stdout, "OnScreenModel::clearOnScreenModel()\n");
	clearModel();
}

void OnScreenModel::clearModel(void)
{
	m_title = "";
	m_type = "";
	m_contents = "";
	m_buttons.clear();
}
