TEMPLATE = app
TARGET = qtshell

QT = qml quick dbus websockets gui-private
CONFIG += c++11 link_pkgconfig wayland-scanner
#DESTDIR = .
DESTDIR = $${OUT_PWD}/../package/root/bin
PKGCONFIG += wayland-client json-c

SOURCES += src/main.cpp \
	   src/onscreenmodel.cpp \
	   src/eventhandler.cpp \
	   src/shell.cpp

HEADERS += src/shell.h \
	   src/onscreenmodel.h \
	   src/eventhandler.h

RESOURCES += qml/qml.qrc images/images.qrc

WAYLANDCLIENTSOURCES += \
    protocol/agl-shell.xml
